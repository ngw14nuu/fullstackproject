export const FETCH_USER = 'FETCH_USER';

export const FETCH_EVENT = 'FETCH_EVENT';
export const FETCH_EVENTS = 'FETCH_EVENTS';

export const FETCH_SQUAD = 'FETCH_SQUAD';
export const FETCH_SQUADS = 'FETCH_SQUADS';
