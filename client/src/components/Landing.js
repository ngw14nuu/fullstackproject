import React from 'react';

const Landing = () => {
  return (
    <div style={{ textAlign: 'center' }}>
      <h1>SquadSquare!</h1>
      Organise your team
    </div>
  );
};

export default Landing;
