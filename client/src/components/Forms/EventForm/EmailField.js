import React from 'react';
import { Input } from 'semantic-ui-react';

export default ({}) => {
  <div>
    <Input icon="users" iconPosition="left" placeholder="Player Name" />
    <Input icon="mail" iconPosition="left" placeholder="Player Email" />
  </div>;
};
